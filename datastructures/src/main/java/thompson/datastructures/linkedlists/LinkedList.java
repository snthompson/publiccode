package thompson.datastructures.linkedlists;

public class LinkedList {
	
	private Node root;
	
	public LinkedList(){
		root = new Node(0);
	}
	
	public Node getRoot() {
		return root;
	}

	public void setRoot(Node root) {
		this.root = root;
	}

	/**
	 * Print out Linked List structure.
	 */
	public void printList(){
		
		Node currNode = root;
		while(currNode != null){
			System.out.print("[Data : " + currNode.getData() + "]" );
			currNode = currNode.getNext();
			if(currNode != null){
				System.out.print(" --> ");
			}
		}
		System.out.println();
	}

}
