package thompson.datastructures.linkedlists;

public class App {
	
public static void main(String[] args){
		
		LinkedList lList = new LinkedList();
		
		Node prevNode = lList.getRoot();
		for(int i = 1; i < 10; i++){
			prevNode.setNext(new Node(i));
			prevNode = prevNode.getNext();
		}
		lList.printList();
		
	}

}
